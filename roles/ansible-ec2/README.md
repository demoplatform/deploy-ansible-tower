ansible-ec2
===========

This role is to manage AWS resources.

Currently limited to single project instacnes. 

#TODO deal with multi ec2 instacne ids?

Requirements
------------

Ensure `ansible-galaxy collection install amazon.aws` is installed.
Ensure `boto3` and `botocore` is installed. 

Role Variables
--------------


Dependencies
------------


Example Playbook
----------------


License
-------

MIT

Author Information
------------------


