<!-- PROJECT LOGO -->
<br />
<p align="center">
  <a href="https://gitlab.consulting.redhat.com/platformdemo/ansible-tower-poc">
    <img src="images/ansiblelogo.png" alt="Logo" width="80" height="80">
  </a>

  <h3 align="center">Ansible Tower Deployment</h3>

  <p align="center">
    Platform Demo
    
  </p>
</p>


<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary><h2 style="display: inline-block">Table of Contents</h2></summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
      </ul>
    </li>
    <li>
      <a href="#usage">Usage</a>
     <ul>
        <li><a href="#install">Install</a></li>
        <li><a href="#inventory">Inventory</a></li>
      </ul>
    </li>
    <li><a href="#letsencrypt">LetsEncrypt</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#acknowledgements">Acknowledgements</a></li>
    
  </ol>
</details>

<!-- ABOUT THE PROJECT -->
## About The Project

Project to demonstrate effective GitLab pipeline using Ansible Tower for deploying Kubernetes and managing deployments. 

The Ansible compositions here are MVP, while including the foundations to be implemented professionally, the minimal configurations are used here in the interest of time. 

The target for the Ansible Tower role is specifically for RHEL 8 in EC2, keeping tasks to a minimum.

The project is using pre-defined Elastic IP and domain openshiftlabs.net, with the following DNS entry:

```sh
tower.openshiftlabs.net A Simple  - 18.134.168.61
```

*See `roles/ansible-X/defaults/main.yml` for variables to override or manage!*

<!-- GETTING STARTED -->
## Getting Started

To use this Ansible repository follow these simple steps.

### Prerequisites

All steps assume a Red hat Enterprise Linux 8 client. 

## Usage

### Install

1. Clone the repo
   ```sh
   git clone git@gitlab.com:demoplatform/deploy-ansible-tower.git
   ```

2. Install Ansible via package management
   ```sh
   $ sudo subscription-manager register --username=username --password=password
   $ sudo subscription-manager attach --auto
   $ sudo dnf update -y
   $ sudo shutdown -r now

   Install Ansible Engine:

   $ sudo subscription-manager repos --enable ansible-2.9-for-rhel-8-x86_64-rpms
   $ sudo dnf install ansible -y
   $ ansible --version
   ansible 2.9.18
   ```

   Or install Ansible using Python Virtual Environment:

   ```sh
   $ python3.8 -m venv venv
   $ source venv/bin/activate
   $ pip install --upgrade pip
   $ pip install ansible
   ```

   To use amazon.aws collection install `boto`:

   ```sh
   $ pip install boto boto3 botocore
   ```

3. Install amazon.aws collection
   
   ```sh
   ansible-galaxy collection install amazon.aws
   ```

### Inventory

The AWS dynamic inventory only needs `aws_ec2` plugin and `regions` defining.

Credentials should be set using environment variables, either create a `.env` file to export them - ensuring they are NOT committed to git. Or export them manually.

```sh
export AWS_ACCESS_KEY_ID='<your_access_key_id>'
export AWS_SECRET_ACCESS_KEY='<your_secret_access_key>'
```

### Playbooks

Quick start, leaving the default variables, a `t3.medium` ec2 instance will be provisioned, and can be terminated:

Create an instance:

```sh
ansible-playbook playbook_manage_ec2_instance_test.yml -e "ansible_ec2_task=create"
```

Terminate an instance:

```sh
ansible-playbook playbook_manage_ec2_instance_test.yml -e "ansible_ec2_task=terminate"
```

The default project tag is set to tower, but this tag variable can be overridden to manage various tagged instances. 

```sh
ansible-playbook playbook_manage_ec2_instance_test.yml -e "ansible_ec2_task=terminate" -e "ansible_ec2_project_tag=test"
```

Working with these roles requires secrets, the following lookups need to be set, again maybe in a `.env` file NOT commited to source control.

```sh
export AWS_ACCESS_KEY_ID='<your_access_key_id>'
export AWS_SECRET_ACCESS_KEY='<your_secret_access_key>'
export REDHAT_SUB_USERNAME='<your_red_hat_subscription_username>'
export REDHAT_SUB_PASSWORD='<your_red_hat_subscription_password>'
export TOWER_ADMIN_PASSWORD='<your_desired_tower_admin_password>'
```

<!-- LETSENCRYPT -->
## LetsEncrypt

### Install Certbot

Encure EPEL is enabled:

```sh
dnf install https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm -y
ARCH=$( /bin/arch )
subscription-manager repos --enable "codeready-builder-for-rhel-8-${ARCH}-rpms"
```

Install snapd:

```sh
dnf install snapd -y
```

Create a soft link:

```sh
ln -s /var/lib/snapd/snap /snap
```

Start the snapd service:

```sh
systemctl enable --now snapd.socket
```

Install and refresh snapd:

```sh
snap install core; snap refresh core
```

Install certbot using snap:

```sh
snap install --classic certbot
```

Create a softlink to the certbot binary:

```sh
ln -s /snap/bin/certbot /usr/bin/certbot
```

Test and view the verison:

```sh
certbot --version
certbot 1.16.0
```

### Tower Certificate

Edit `/etc/nginx/nginx.conf`:

```sh
vi /etc/nginx/nginx.conf
```

Change:

```sh
    server_name _;
```

To the domain used for the Ansible Tower deployment, for example:

```sh
    server_name tower.exampleforyou.net;
```

Reload the NGINX configuration:

```sh
systemctl reload nginx.service
```

Now, run `certbot`:

```sh
certbot --nginx
```

You will be asked various questions, the key part is it picking up the domain name previously configured for tower, so `certbot` can generate the Let Encrypt certificate.

```sh
Which names would you like to activate HTTPS for?
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
1: tower.exampleforyou.net
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Select the appropriate numbers separated by commas and/or spaces, or leave input
blank to select all options shown (Enter 'c' to cancel): 1
Requesting a certificate for tower.exampleforyou.net

Successfully received certificate.
Certificate is saved at: /etc/letsencrypt/live/tower.exampleforyou.net/fullchain.pem
Key is saved at:         /etc/letsencrypt/live/tower.exampleforyou.net/privkey.pem
This certificate expires on 2021-09-05.
These files will be updated when the certificate renews.
Certbot has set up a scheduled task to automatically renew this certificate in the background.

Deploying certificate
Successfully deployed certificate for tower.exampleforyou.net to /etc/nginx/nginx.conf
Congratulations! You have successfully enabled HTTPS on https://tower.exampleforyou.net
```

Finally, restart Ansible Tower:

```sh
# ansible-tower-service restart
```

<!-- LICENSE -->
## License

Distributed under the MIT License. See `LICENSE` for more information.


<!-- CONTACT -->
## Contact

Richard Walker - rwalker@redhat.com


<!-- ACKNOWLEDGEMENTS -->
## Acknowledgements

* Richard Walker
* Mike Croft
* Justin Davis
* Nicholas Woods

## TODO

* Roles for monitoring stack
* Tower API call PoC from GitLab pipeline

