Create a `.env` file in this directory containing and export it `source .env`.

```sh
export AWS_ACCESS_KEY_ID='<access_key_id>'
export AWS_SECRET_ACCESS_KEY='<access_key>'
```